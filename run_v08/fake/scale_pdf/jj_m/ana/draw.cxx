void draw()
{
	vector<TH1D*> vec_hsr;
	vector<double> vec_bin1,vec_bin2,vec_bin3,vec_bin4;
	ifstream data("list.txt");
	while(!data.eof())
	{
		string temp;
		data>>temp;
		if(temp=="")continue;
		//if(temp.find("_scale_")!=string::npos)continue;
		if(temp.find("_pdf_")!=string::npos)continue;
		TFile *f=TFile::Open(TString(temp),"READ");
		if(!f->IsOpen())
		{
			cout<<temp<<" could not be opened"<<endl;
			return;
		}
		TH1D *htmp = (TH1D*)f->Get("hfakesr");
		htmp->SetDirectory(0);
		vec_hsr.push_back(htmp);
		f->Close();
		delete f;
	}
	TCanvas *c1=new TCanvas("c1","scale",1024,768);
	c1->cd();
	for(auto i:vec_hsr)
	{
		if((find(vec_hsr.begin(),vec_hsr.end(),i)-vec_hsr.begin())==0)i->Draw("HISTE0");
		else i->Draw("HISTSAMEE0");
		vec_bin1.push_back(i->GetBinContent(1));
		vec_bin2.push_back(i->GetBinContent(2));
		vec_bin3.push_back(i->GetBinContent(3));
		vec_bin4.push_back(i->GetBinContent(4));
	}
	double sum1=0,sum2=0,sum3=0,sum4=0;
	double mean1=0,mean2=0,mean3=0,mean4=0;
	double srsum1=0,srsum2=0,srsum3=0,srsum4=0;
	for(int i=0;i<vec_bin1.size();i++)
	{
		sum1+=vec_bin1.at(i);
		sum2+=vec_bin2.at(i);
		sum3+=vec_bin3.at(i);
		sum4+=vec_bin4.at(i);
	}
	mean1=sum1/vec_bin1.size();
	mean2=sum2/vec_bin2.size();
	mean3=sum3/vec_bin3.size();
	mean4=sum4/vec_bin4.size();
	for(int i=0;i<vec_bin1.size();i++)
	{
		srsum1+=pow(vec_bin1.at(i)-mean1,2);
		srsum2+=pow(vec_bin2.at(i)-mean2,2);
		srsum3+=pow(vec_bin3.at(i)-mean3,2);
		srsum4+=pow(vec_bin4.at(i)-mean4,2);
	}
	srsum1 = sqrt(srsum1)/mean1;
	srsum2 = sqrt(srsum2)/mean2;
	srsum3 = sqrt(srsum3)/mean3;
	srsum4 = sqrt(srsum4)/mean4;
	cout<<srsum1<<" "<<srsum2<<" "<<srsum3<<" "<<srsum4<<endl;
}
