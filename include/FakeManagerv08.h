#ifndef FAKEMANAGERV08_HH
#define FAKEMANAGERV08_HH

#include <fstream>
#include <string>
#include <TH2D.h>
#include <vector>
#include <map>
#include "TFile.h"
#include "TStyle.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TH2.h"
#include "TH1.h"
#include "TH1D.h"
#include "TLorentzVector.h"
#include "TVectorD.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TROOT.h"
#include <atomic>
#include <mutex>
#include <future>
#include <thread>
#include <utility>
#include <chrono>
#include <iomanip>


using namespace std;

class FakeManagerv08{
	public:
		// Variables for factor calculation from dijet
		const int nptbin=3;
		const double pt_bin[4]={20,40,140,1000};
		const double pt_bin9[10]={20,40,50,60,70,80,90,100,140,1000};
		const int netabin = 6;
		const double eta_bin[7] = {0, 0.5, 1, 1.37, 1.52, 2.0, 2.5};

		FakeManagerv08();
		~FakeManagerv08();
		void Calfactor_Dijet(const string &list,double weight_cut);
		void Calfactor_Gamjet(const string &list);
		void Fake(const string &list,const TString varname,const int _nbin,const double _bin[],const int scale_pdf=0,const int sp_index=0);
		void Syst();
		void Plot();
		void SetVar(const string var){_varname = var;};
	private:
		string _varname;
		enum EventSelections {
			all = long(1) << 0,
			fjvt = long(1) << 1,
			init = long(1) << 2,
			trigger = long(1) << 3,
			primary_vertex = long(1) << 4,
			dq = long(1) << 5,
			duplicate = long(1) << 6,
			jetclean = long(1) << 7,
			onelep = long(1) << 8,
			metcut = long(1) << 9,
			onegam = long(1) << 10,
			twojet = long(1) << 11,
			mjj = long(1) << 12,
			second_lepton_veto = long(1) << 13,
			ly_Z_veto = long(1) << 14,
			dphi = long(1) << 15,
			dr = long(1) << 16,
			onelep_truth = long(1) << 17,
			onegam_truth = long(1) << 18,
			trigger_pt_truth = long(1) << 19,
			ly_Z_veto_truth = long(1) << 20,
			twojet_truth = long(1) << 21,
			dphi_truth = long(1) << 22,
			dr_truth = long(1) << 23,
			vy_OR = long(1) << 24,
			Wenu = long(1) << 25,
			Wmunu = long(1) << 26,
			Wenu_truth = long(1) << 27,
			Wmunu_truth = long(1) << 28,
		};

		enum ObjectSelections {
			triggermatch_e = long(1) << 1,
			triggermatch_e2 = long(1) << 2,
			triggermatch_e3 = long(1) << 3,
			triggermatch_mu = long(1) << 4,
			triggermatch_mu2 = long(1) << 5,
			triggermatch_mu3 = long(1) << 6,
			bm_e_passIP = long(1) << 7,
			bm_e2_passIP = long(1) << 8,
			bm_e3_passIP = long(1) << 9,
			bm_mu_passIP = long(1) << 10,
			bm_mu2_passIP = long(1) << 11,
			bm_mu3_passIP = long(1) << 12,
			bm_gam_idLoose = long(1) << 13,
			bm_gam_idTight = long(1) << 14,
			bm_gam_isoTight = long(1) << 15,
			bm_gam_isoLoose = long(1) << 16,
			bm_gam_isLoosePrime2 = long(1) << 17,
			bm_gam_isLoosePrime3 = long(1) << 18,
			bm_gam_isLoosePrime4a = long(1) << 19,
			bm_gam_isLoosePrime5 = long(1) << 20,
			bm_mu_idLoose = long(1) << 21,
			bm_mu_idMedium = long(1) << 22,
			bm_mu_idTight = long(1) << 23,
			bm_mu_isoPflowLoose_VarRad = long(1) << 24,
			bm_mu_isoPflowTight_VarRad = long(1) << 25,
			bm_mu2_idLoose = long(1) << 26,
			bm_mu2_idMedium = long(1) << 27,
			bm_mu2_idTight = long(1) << 28,
			bm_mu2_isoPflowLoose_VarRad = long(1) << 29,
			bm_mu2_isoPflowTight_VarRad = long(1) << 30,
			bm_mu3_idLoose = long(1) << 31,
			bm_mu3_idMedium = long(1) << 32,
			bm_mu3_idTight = long(1) << 33,
			bm_mu3_isoPflowLoose_VarRad = long(1) << 34,
			bm_mu3_isoPflowTight_VarRad = long(1) << 35,
			bm_e_idLoose = long(1) << 35,
			bm_e_idMedium = long(1) << 36,
			bm_e_idTight = long(1) << 37,
			bm_e_isoLooseVarRad = long(1) << 38,
			bm_e_isoTightVarRad = long(1) << 39,
			bm_e2_idLoose = long(1) << 40,
			bm_e2_idMedium = long(1) << 41,
			bm_e2_idTight = long(1) << 42,
			bm_e2_isoLooseVarRad = long(1) << 43,
			bm_e2_isoTightVarRad = long(1) << 44,
			bm_e3_idLoose = long(1) << 45,
			bm_e3_idMedium = long(1) << 46,
			bm_e3_idTight = long(1) << 47,
			bm_e3_isoLooseVarRad = long(1) << 47,
			bm_e3_isoTightVarRad = long(1) << 48,
		};
		enum Region {
			baseline_e = long(1) << 0,
			baseline_mu = long(1) << 1,
			sr_y = long(1) << 2, 
			sr_ly = long(1) << 3, 
			sr_wy = long(1) << 4, 
			cra_y = long(1) << 5,
			cra_ly = long(1) << 6, 
			cra_wy = long(1) << 7,
			crb_y = long(1) << 8,
			crb_ly = long(1) << 9,
			crb_wy = long(1) << 10,
			crc_y = long(1) << 11,
			crc_ly = long(1) << 12,
			crc_wy = long(1) << 13,
			sr_y_truth = long(1) << 14, 
			sr_ly_truth = long(1) << 15, 
			sr_wy_truth = long(1) << 16, 
			cra_y_truth = long(1) << 17,
			cra_ly_truth = long(1) << 18, 
			cra_wy_truth = long(1) << 19,
			crb_y_truth = long(1) << 20,
			crb_ly_truth = long(1) << 21,
			crb_wy_truth = long(1) << 22,
			crc_y_truth = long(1) << 23,
			crc_ly_truth = long(1) << 24,
			crc_wy_truth = long(1) << 25,
			baseline_e_loosey = long(1) << 26,
			baseline_mu_loosey = long(1) << 27,
			loosey_lp2 = long(1) << 28,
			loosey_lp3 = long(1) << 29,
			loosey_lp4 = long(1) << 30,
			loosey_lp5 = long(1) << 31,
			loosey_idtight = long(1) << 32,
			reco = long(1) << 33,
			truth = long(1) << 34,
			baseline_e_truth = long(1) << 35,
			baseline_mu_truth = long(1) << 36,
			baseline_e_invb = long(1) << 37,
			baseline_mu_invb = long(1) << 38,
			baseline_e_loosey_invb = long(1) << 39,
			baseline_mu_loosey_invb = long(1) << 40,
			baseline_e_truth_invb = long(1) << 41,
			baseline_mu_truth_invb = long(1) << 42,
		};



};


#endif
