#ifndef FAKEMANAGER_HH
#define FAKEMANAGER_HH

#include <fstream>
#include <string>
#include <TH2D.h>
#include <vector>
#include <map>
#include "TFile.h"
#include "TStyle.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TH2.h"
#include "TH1.h"
#include "TH1D.h"
#include "TLorentzVector.h"
#include "TVectorD.h"
#include "TLatex.h"
#include "TLegend.h"

using namespace std;

class FakeManager{
public:
	// Variables for factor calculation from dijet
	const int nptbin=3;
	const double pt_bin[4]={20,40,140,1000};
	const double pt_bin9[10]={20,40,50,60,70,80,90,100,140,1000};
	const int netabin = 6;
	const double eta_bin[7] = {0, 0.5, 1, 1.37, 1.52, 2.0, 2.5};
	
	FakeManager();
	~FakeManager();
	void Calfactor_Dijet(const string &list,double weight_cut);
	void Calfactor_Gamjet(const string &list);
	void Fake(const string &list,const TString varname,const int _nbin,const double _bin[]);
	void Syst();
	void Plot();
	void SetVar(const string var){_varname = var;};
private:
	string _varname;
	
};


#endif
