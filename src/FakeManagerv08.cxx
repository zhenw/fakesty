#include "FakeManagerv08.h"
#include <TH2.h>
#include <iostream>
#include <sstream>
#include <algorithm>

using namespace std;

void FakeManagerv08::Fake(const string &list,const TString varname,const int _nbin,const double _bin[],const int scale_pdf,const int sp_index)
{
	//ROOT::EnableImplicitMT();
	ROOT::DisableImplicitMT();
	//Read fake factor from gam jet and dijet files
	TFile *fgam=TFile::Open("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/gamjet/v8/ffip_jpt30.root","READ");
	//cout<<"gam jet fake factor read!"<<endl;
	TH1F *hgam_ff=(TH1F*)fgam->Get("hptff");
	TFile *fmc=TFile::Open("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/dijet/fakefactor_met20_wmt20_jpt30.root","READ");
	TH1F *hmcpt=(TH1F*)fmc->Get("hfptdj");
	TH1F *hdjpt=(TH1F*)fmc->Get("hfpt");
	//....
	//Variables variation systematics
	TString sys_cut_filename[6]={"fakefactor_met15_wmt20_jpt30.root","fakefactor_met25_wmt20_jpt30.root","fakefactor_met20_wmt15_jpt30.root","fakefactor_met20_wmt25_jpt30.root","fakefactor_met20_wmt20_jpt25.root","fakefactor_met20_wmt20_jpt35.root"};
	TFile *fvar[6];TH1F *hvarsys[6];
	TH1F *hvar=new TH1F("hvar","test",nptbin,pt_bin);
	for(int i=0;i<6;++i)fvar[i]=TFile::Open("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/dijet/"+sys_cut_filename[i],"READ");
	for(int i=0;i<6;++i)
	{
		hvarsys[i]=(TH1F*)fvar[i]->Get("hfpt");
		for(int j=1;j<=nptbin;++j)
		{
			double nominal=hdjpt->GetBinContent(j);
			double current=hvarsys[i]->GetBinContent(j);
			if(hvar->GetBinContent(j)<(abs(current-nominal)))hvar->SetBinContent(j,abs(current-nominal));
		}
	}
	//...
	// Gam jet dijet closure systematic
	TH1F *hr=new TH1F("hr","test",nptbin,pt_bin);
	for(int i=0;i<hr->GetNbinsX();++i)
	{
		double dj=0,yj=0,dif=0;
		dj=hmcpt->GetBinContent(i+1);
		yj=hgam_ff->GetBinContent(i+1);
		dif= abs(dj-yj);
		hr->SetBinError(i+1,0.);
		hr->SetBinContent(i+1,dif);
	}
	//Systematic values
	double sys[3]={hr->GetBinContent(1),hr->GetBinContent(2),hr->GetBinContent(3)};
	double varsys[3]={hvar->GetBinContent(1),hvar->GetBinContent(2),hvar->GetBinContent(3)};

	auto f = [this,list,hr,hvar,varsys,sys](const double ptbin[],const int &size_ptbin,const TString var_name,const int &nbin,const double bin[],
			const int &systematic_gamjet,
			const int &systematic_qcd, const int &systematic_var, const int &systematic_scale=0, const int &systematic_pdf=0)->void
	{
		//...
		//Read 2d fake factors
		TFile *f_ff=nullptr;
		if(size_ptbin==3)f_ff =TFile::Open("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/dijet/fakefactor_met20_wmt20_jpt30.root", "READ");
		else f_ff =TFile::Open("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/dijet/fakefactor_npt9.root","READ");
		TH2F *h2eff = (TH2F *) f_ff->Get("h2eff");
		//Calculate fake estimations
		ifstream sow("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/dijet/sow.txt");
		unordered_map<int,int> mappid={{36207,0},{44307,1},{58450,2}};
		unordered_map<int,double[3]> mapsow={};
		while(!sow.eof())
		{
			int dsid1;
			double sowa,sowd,sowe;
			sow>>dsid1>>sowa>>sowd>>sowe;
			if(dsid1==0)continue;
			mapsow[dsid1][0]=sowa;
			mapsow[dsid1][1]=sowd;
			mapsow[dsid1][2]=sowe;
		}
		ifstream data(list);
		double luminosity, xsection, geneff, kfactor, sum_of_weights;
		Double_t dsid;
		TH1F *hqcderrsr = new TH1F("hqcderrsr", "SR", nbin, bin);
		TH1F *hqcderrcra = new TH1F("hqcderrcra", "CRA", nbin, bin);
		TH1F *hqcderrcrb = new TH1F("hqcderrcrb", "CRB", nbin, bin);
		TH1F *hqcderrcrc = new TH1F("hqcderrcrc", "CRC", nbin, bin);
		TH1F *hshsr = new TH1F("hshsr","SR",nbin,bin);
		TH1F *hshcra = new TH1F("hshcra","CRA",nbin,bin);
		TH1F *hshcrb = new TH1F("hshcrb","CRB",nbin,bin);
		TH1F *hshcrc = new TH1F("hshcrc","CRC",nbin,bin);
		TH1F *hmgsr = new TH1F("hmgsr","SR",nbin,bin);
		TH1F *hmgcra = new TH1F("hmgcra","CRA",nbin,bin);
		TH1F *hmgcrb = new TH1F("hmgcrb","CRB",nbin,bin);
		TH1F *hmgcrc = new TH1F("hmgcrc","CRC",nbin,bin);
		TH1F *hsr = new TH1F("hsr", "SR", nbin, bin);
		TH1F *hcra = new TH1F("hcra", "CRA", nbin, bin);
		TH1F *hcrb = new TH1F("hcrb", "CRB", nbin, bin);
		TH1F *hcrc = new TH1F("hcrc", "CRC", nbin, bin);
		TH1F *hqcdsr = new TH1F("hqcdsr", "SR", nbin, bin);
		TH1F *hqcdcra = new TH1F("hqcdcra", "CRA", nbin, bin);
		TH1F *hqcdcrb = new TH1F("hqcdcrb", "CRB", nbin, bin);
		TH1F *hqcdcrc = new TH1F("hqcdcrc", "CRC", nbin, bin);
		TH1F *hmcsr = new TH1F("hmcsr", "MCSR", nbin, bin);
		TH1F *hmccra = new TH1F("hmccra", "MCCRA", nbin, bin);
		TH1F *hmccrb = new TH1F("hmccrb", "MCCRB", nbin, bin);
		TH1F *hmccrc = new TH1F("hmccrc", "MCCRC", nbin, bin);
		hsr->Sumw2();hcra->Sumw2();hcrb->Sumw2();hcrc->Sumw2();
		hmcsr->Sumw2();hmccra->Sumw2();hmccrb->Sumw2();hmccrc->Sumw2();
		hshsr->Sumw2();hshcra->Sumw2();hshcrb->Sumw2();hshcrc->Sumw2();
		hmgsr->Sumw2();hmgcra->Sumw2();hmgcrb->Sumw2();hmgcrc->Sumw2();
		while(!data.eof())
		{
			string temp;
			data>>temp;
			if (temp == "")continue;
			//cout<<temp<<" Starting"<<endl;
			TFile *f = TFile::Open(TString(temp),"READ");
			if(!f->IsOpen())
			{
				cout<<temp<<" could not be opened"<<endl;
				return;
			}
			TTree *t = (TTree *) f->Get("vbswy");
			Double_t        weight_all;
			Double_t        weight_mc;
			Float_t        lep_pt;
			Float_t        e_pt;
			Float_t        e_eta;
			Float_t        met;
			Float_t        lep_eta;
			Float_t        gam_eta;
			Float_t        gam_pt;
			Float_t        w_mt;
			Float_t        j0_pt;
			Float_t        j1_pt;
			Int_t           n_bjets_85;
			Float_t        jj_drap;
			Float_t        jj_m;
			Float_t        jj_pt;
			Float_t         jj_dphi_signed;
			Int_t           e_truth_type;
			Int_t           n_gapjets;
			Int_t           e_truth_origin;
			Float_t         nn_score;
			ULong64_t       selection_bits;
			ULong64_t       object_bits;
			ULong64_t       region_bits;
			Float_t         wy_xi;
			vector<double>  *th_syst_weights;
			th_syst_weights=0;
			t->SetBranchAddress("lep_pt", &lep_pt);
			t->SetBranchAddress("met", &met);
			t->SetBranchAddress("lep_eta", &lep_eta);
			t->SetBranchAddress("e_pt", &e_pt);
			t->SetBranchAddress("e_eta", &e_eta);
			t->SetBranchAddress("gam_eta", &gam_eta);
			t->SetBranchAddress("gam_pt", &gam_pt);
			t->SetBranchAddress("w_mt", &w_mt);
			t->SetBranchAddress("j0_pt", &j0_pt);
			t->SetBranchAddress("j1_pt", &j1_pt);
			t->SetBranchAddress("n_bjets_85", &n_bjets_85);
			t->SetBranchAddress("jj_drap", &jj_drap);
			t->SetBranchAddress("jj_m", &jj_m);
			t->SetBranchAddress("jj_pt", &jj_pt);
			t->SetBranchAddress("jj_dphi_signed", &jj_dphi_signed);
			t->SetBranchAddress("region_bits", &region_bits);
			t->SetBranchAddress("selection_bits", &selection_bits);
			t->SetBranchAddress("object_bits", &object_bits);
			t->SetBranchAddress("e_truth_type", &e_truth_type);
			t->SetBranchAddress("n_gapjets", &n_gapjets);
			t->SetBranchAddress("wy_xi", &wy_xi);
        t->SetBranchAddress("weight_all", &weight_all);
        t->SetBranchAddress("weight_mc", &weight_mc);
        t->SetBranchAddress("e_truth_origin", &e_truth_origin);
			t->SetBranchAddress("nn_score", &nn_score);
			t->SetBranchAddress("th_syst_weights", &th_syst_weights);
			TTree *t2=(TTree*)f->Get("DAOD_tree");
			t2->SetBranchAddress("dsid", &dsid);
			t2->SetBranchAddress("luminosity",&luminosity);
			t2->SetBranchAddress("xsection",&xsection);
			t2->SetBranchAddress("geneff",&geneff);
			t2->SetBranchAddress("kfactor",&kfactor);
			t2->GetEntry(0);
			bool ispj = (dsid>=364541 && dsid<=364547);
			bool issh = (dsid >= 700398 && dsid <= 700404);
			bool isoldqcd=(dsid>=700015 && dsid<=700017);
			bool ismg = (dsid >= 504678 && dsid <= 504680);
			bool isoldwj=(dsid>=364156&&dsid<=364197);
			bool isoldzj=(dsid>=364100&&dsid<=364141);
			bool isoldzy=(dsid>=700011&&dsid<=700013);
			bool isoldewk = (dsid >= 363270 && dsid <= 363272);
			bool isewk = (dsid ==700707 || dsid == 700709);
			bool iswj = (dsid>=700320 && dsid<=700334);
			bool iszj = (dsid>=700335 && dsid<=700349);
			bool ismc = (dsid > 1e5);
			bool isdata = (dsid==0);
			bool isdijet = (dsid >= 364701 && dsid <= 364712);
			bool istop = (dsid == 410470 || dsid == 410471 || dsid == 500334 || dsid ==412006 || dsid == 410389) || (dsid >= 410644 && dsid <=410659);
			if (ispj || isoldqcd || isoldwj || isoldzj || isoldzy || isoldewk)
			{
				f->Close();
				continue;
			}
			int Nentry = t->GetEntries();
			for(int j=0;j<Nentry;j++)
			{
				t->GetEntry(j);
				double ff = 0.,var=0.;
				if(systematic_scale && th_syst_weights->size()>=317 && (iswj || iszj)) weight_mc = th_syst_weights->at(systematic_scale);
				if(systematic_pdf && th_syst_weights->size()>=317 && (iswj || iszj)) weight_mc = th_syst_weights->at(systematic_pdf);
				double ew = weight_all*weight_mc*geneff*kfactor*xsection*luminosity/mapsow[int(dsid)][mappid[int(luminosity)]];
				bool isreal = (e_truth_type >= 1 && e_truth_type <= 4);
				//bool issr= true;
				bool issr = (n_gapjets==0 && wy_xi<=0.35);
				bool iscra = (n_gapjets>=1 && wy_xi<=0.35);
				bool iscrb = (n_gapjets>=1 && wy_xi>0.35 && wy_xi<=1.0);
				bool iscrc = (n_gapjets==0 && wy_xi>0.35 && wy_xi<=1.0);
				//bool issr = (region_bits & Region::sr_wy) != 0;
				//bool iscra = (region_bits & Region::cra_wy) != 0;
				//bool iscrb = (region_bits & Region::crb_wy) != 0;
				//bool iscrc = (region_bits & Region::crc_wy) != 0;
				bool pass_onegam = (selection_bits & EventSelections::onegam )!= 0;
				bool pass_trigger = (selection_bits & EventSelections::trigger )!= 0;
				bool pass_twojet = (selection_bits & EventSelections::twojet )!= 0;
				bool pass_primary_vertex = (selection_bits & EventSelections::primary_vertex )!= 0;
				bool pass_duplicate = (selection_bits & EventSelections::duplicate )!= 0;
				bool pass_dq = (selection_bits & EventSelections::dq )!= 0;
				bool pass_jetclean = (selection_bits & EventSelections::jetclean )!= 0;
				bool pass_vy_OR = (selection_bits & EventSelections::vy_OR )!= 0;
				bool is_Wenu = (selection_bits & EventSelections::Wenu )!= 0;
				bool pass_onelep = (selection_bits & EventSelections::onelep )!= 0;
				bool pass_second_lepton_veto = (selection_bits & EventSelections::second_lepton_veto )!= 0;
				bool pass_ly_Z_veto = (selection_bits & EventSelections::ly_Z_veto )!= 0;
				bool pass_dphi = (selection_bits & EventSelections::dphi )!= 0;
				bool pass_dr = (selection_bits & EventSelections::dr )!= 0;
				bool e_passIP = (object_bits & ObjectSelections::bm_e_passIP )!= 0;
				bool gam_idTight = (object_bits & ObjectSelections::bm_gam_idTight )!= 0;
				bool gam_isoTight = (object_bits & ObjectSelections::bm_gam_isoTight )!= 0;
				bool e_isoLooseVarRad = ((object_bits & ObjectSelections::bm_e_isoLooseVarRad )!= 0);
            bool e_isoTightVarRad = ((object_bits & ObjectSelections::bm_e_isoTightVarRad )!= 0);
            bool e_idMedium = ((object_bits & ObjectSelections::bm_e_idMedium )!= 0);
            bool e_idTight = ((object_bits & ObjectSelections::bm_e_idTight )!= 0);
				bool baseline = ((pass_onegam) && (pass_trigger) && (pass_twojet) && (pass_primary_vertex) && (pass_duplicate) &&
						(pass_dq) && (pass_jetclean) && (pass_vy_OR) && (is_Wenu) && (pass_onelep) &&
						(gam_idTight) && (gam_isoTight) && (pass_second_lepton_veto) && (gam_pt > 22.0) &&
						(lep_pt > 30.0) && (met > 30.0) && (lep_eta > -2.5) && (lep_eta < 2.5) &&
						(gam_eta > -2.37) && (gam_eta < 2.37) && (w_mt > 30.0) && (pass_ly_Z_veto) &&
						(pass_dphi) && (pass_dr) && (j0_pt > 50.0) && (j1_pt > 50.0) && (n_bjets_85 == 0) &&
						(jj_drap > 2.) && (e_passIP) && jj_m>1000.);
				bool pass_signal_e = baseline && e_isoTightVarRad && e_idTight;
				if(var_name == "jj_m")var=jj_m;
				else if(var_name == "jj_dphi_signed")var=jj_dphi_signed;
				else if(var_name == "ngapjets")var=n_gapjets;
				else if(var_name == "nn")var=nn_score;
				else if(var_name == "jj_pt")var=jj_pt;
				else
				{
					cout<<"Error! No variables found! Existing! "<<endl;
					return;
				}
				if(var>bin[nbin])var=bin[nbin]-0.01; // Overflow correction
				if (isdata)ew = 1.0;
				if (!baseline)continue;
				for(int ipt=0;ipt<size_ptbin;++ipt)
				{
					for(int ieta=0;ieta<netabin;++ieta)
					{
						if(abs(e_eta) > eta_bin[ieta] && abs(e_eta) < eta_bin[ieta+1])
						{
							if(e_pt > ptbin[ipt] && e_pt < ptbin[ipt+1])
							{
								ff = h2eff->GetBinContent(ipt+1,ieta+1);
								if(systematic_var==1)ff*=(1+varsys[ipt]);
								else if(systematic_var==-1)ff*=(1-varsys[ipt]);
								if(systematic_gamjet==1)ff*=(1+sys[ipt]);
								else if(systematic_gamjet==-1)ff*=(1-sys[ipt]);
								ew *= ff;
							}
						}
					}
				}
				if (!pass_signal_e) {
					if (ismc) {
						if (abs(ew)>20) {
							cout<<dsid<<" "<<var<<" "<<ew<<endl;
						} else if (issh) {
							if (issr && isreal)hshsr->Fill(var, ew);
							else if (iscra && isreal)hshcra->Fill(var, ew);
							else if (iscrb && isreal)hshcrb->Fill(var, ew);
							else if (iscrc && isreal)hshcrc->Fill(var, ew);
							else {}
						} else if (ismg) {
							if (issr && isreal)hmgsr->Fill(var, ew);
							else if (iscra && isreal)hmgcra->Fill(var, ew);
							else if (iscrb && isreal)hmgcrb->Fill(var, ew);
							else if (iscrc && isreal)hmgcrc->Fill(var, ew);
							else {}
						} else {
							if (issr && isreal)hmcsr->Fill(var, ew);
							else if (iscra && isreal)hmccra->Fill(var, ew);
							else if (iscrb && isreal)hmccrb->Fill(var, ew);
							else if (iscrc && isreal)hmccrc->Fill(var, ew);
							else {}
						}
					} else {
						if (issr)hsr->Fill(var, ew);
						else if (iscra)hcra->Fill(var, ew);
						else if (iscrb)hcrb->Fill(var, ew);
						else if (iscrc)hcrc->Fill(var, ew);
						else {}
					}
				}

			}
			f->Close();
		}
		// List if looped done
		TFile *fout;
		if(size_ptbin==3){
			if(systematic_gamjet==0 && systematic_qcd==0 && systematic_var==0 && systematic_scale==0 && systematic_pdf==0){fout=new TFile("fake_nominal.root","RECREATE");}
			else if(systematic_gamjet==1){fout=new TFile("fake_gamjetup.root", "RECREATE");}
			else if(systematic_gamjet==-1){fout=new TFile("fake_gamjetdown.root", "RECREATE");}
			else if(systematic_qcd==1){fout=new TFile("fake_qcd.root","RECREATE");}
			else if(systematic_var==1){fout=new TFile("fake_varup.root","RECREATE");}
			else if(systematic_var==-1){fout=new TFile("fake_vardown.root","RECREATE");}
			else if(systematic_scale){fout = new TFile("fake_scale_"+TString(to_string(systematic_scale))+".root","RECREATE");}
			else if(systematic_pdf){fout = new TFile("fake_pdf_"+TString(to_string(systematic_pdf))+".root","RECREATE");}
			else {}
		}
		else if(size_ptbin==9)
		{
			fout=new TFile("fake_npt9.root","RECREATE");
		}
		if(systematic_qcd==0)
		{
			hqcdsr=(TH1F*)hshsr->Clone("hqcdsr");
			hqcdcra=(TH1F*)hshcra->Clone("hqcdcra");
			hqcdcrb=(TH1F*)hshcrb->Clone("hqcdcrb");
			hqcdcrc=(TH1F*)hshcrc->Clone("hqcdcrc");
		}
		else
		{
			hqcdsr=(TH1F*)hmgsr->Clone("hqcdsr");
			hqcdcra=(TH1F*)hmgcra->Clone("hqcdcra");
			hqcdcrb=(TH1F*)hmgcrb->Clone("hqcdcrb");
			hqcdcrc=(TH1F*)hmgcrc->Clone("hqcdcrc");
		}
		hmcsr->Add(hqcdsr);
		hmccra->Add(hqcdcra);
		hmccrb->Add(hqcdcrb);
		hmccrc->Add(hqcdcrc);

		hsr->Add(hmcsr,-1);
		hcra->Add(hmccra,-1);
		hcrb->Add(hmccrb,-1);
		hcrc->Add(hmccrc,-1);
		for(int ibin=0;ibin<hsr->GetNbinsX();ibin++)
		{
			if(hsr->GetBinContent(ibin+1)<0.)hsr->SetBinContent(ibin+1,0.);
			if(hcra->GetBinContent(ibin+1)<0.)hcra->SetBinContent(ibin+1,0.);
			if(hcrb->GetBinContent(ibin+1)<0.)hcrb->SetBinContent(ibin+1,0.);
			if(hcrc->GetBinContent(ibin+1)<0.)hcrc->SetBinContent(ibin+1,0.);
		}
		fout->cd();
		hsr->Write("hfakesr");
		hcra->Write("hfakecra");
		hcrb->Write("hfakecrb");
		hcrc->Write("hfakecrc");
		fout->Close();
		cout<<fout->GetName()<<" Generated!"<<endl;
	};
	//Scale variations following
	//for(int scale=5;scale<=17;scale+=2)
	//{
	//	f(pt_bin,nptbin,varname,_nbin,_bin,0,0,0,scale);
	//}
	////PDF variations following
	//for(int pdf=19;pdf<=317;pdf+=2)
	//{
	//	f(pt_bin,nptbin,varname,_nbin,_bin,0,0,0,0,pdf);
	//}
	if(scale_pdf==1)
	{
		if(sp_index>=5 && sp_index<=17)f(pt_bin,nptbin,varname,_nbin,_bin,0,0,0,sp_index);
		if(sp_index>=19 && sp_index<=317)f(pt_bin,nptbin,varname,_nbin,_bin,0,0,0,0,sp_index);
	}
	else
	{
		f(pt_bin,nptbin,varname,_nbin,_bin,0,0,0);
		f(pt_bin,nptbin,varname,_nbin,_bin,1,0,0);
		f(pt_bin,nptbin,varname,_nbin,_bin,-1,0,0);
		f(pt_bin,nptbin,varname,_nbin,_bin,0,1,0);
		f(pt_bin,nptbin,varname,_nbin,_bin,0,0,1);
		f(pt_bin,nptbin,varname,_nbin,_bin,0,0,-1);
		f(pt_bin9,9,varname,_nbin,_bin,0,0,0);
	}
	
	
}

void FakeManagerv08::Calfactor_Gamjet(const string &list)
{
	ifstream data(list);
	TH1F *pjtt=new TH1F("pjtt","truth_type",38,0,38);
	TH1F *pjto=new TH1F("pjto","truth_origin",45,0,45);
	TH1F *hptb=new TH1F("hptb","PJ Baseline",nptbin,pt_bin);
	TH1F *hpts=new TH1F("hpts","PJ Signal",nptbin,pt_bin);
	double luminosity, xsection, geneff, kfactor, sum_of_weights;
	Double_t dsid;
	ifstream sow("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/dijet/sow.txt");
	unordered_map<int,int> mappid={{36207,0},{44307,1},{58450,2}};
	unordered_map<int,double[3]> mapsow={};
	while(!sow.eof())
	{
		int dsid1;
		double sowa,sowd,sowe;
		sow>>dsid1>>sowa>>sowd>>sowe;
		if(dsid1==0)continue;
		mapsow[dsid1][0]=sowa;
		mapsow[dsid1][1]=sowd;
		mapsow[dsid1][2]=sowe;
	}
	while(!data.eof())
	{
		string temp;
		data>>temp;
		if (temp == "")continue;
		TFile *f = TFile::Open(TString(temp),"READ");
		TTree *t = (TTree *) f->Get("vbswy");
		Double_t        weight_all;
		Double_t        weight_mc;
		Float_t        lep_pt;
		Float_t        met;
		Float_t        lep_eta;
		Float_t        gam_eta;
		Float_t        gam_pt;
		Float_t        w_mt;
		Float_t        j0_pt;
		Float_t        j1_pt;
		Int_t           n_bjets_85;
		Float_t        jj_drap;
		Float_t        jj_m;
		Float_t         e_pt;
		Float_t         e_eta;
		Int_t           e_truth_type;
		Int_t           e_truth_origin;
		ULong64_t       selection_bits;
		ULong64_t       object_bits;
		ULong64_t       region_bits;
		t->SetBranchAddress("region_bits", &region_bits);
		t->SetBranchAddress("selection_bits", &selection_bits);
		t->SetBranchAddress("object_bits", &object_bits);
		t->SetBranchAddress("lep_pt", &lep_pt);
		t->SetBranchAddress("met", &met);
		t->SetBranchAddress("lep_eta", &lep_eta);
		t->SetBranchAddress("gam_eta", &gam_eta);
		t->SetBranchAddress("gam_pt", &gam_pt);
		t->SetBranchAddress("w_mt", &w_mt);
		t->SetBranchAddress("j0_pt", &j0_pt);
		t->SetBranchAddress("j1_pt", &j1_pt);
		t->SetBranchAddress("n_bjets_85", &n_bjets_85);
		t->SetBranchAddress("jj_drap", &jj_drap);
		t->SetBranchAddress("jj_m", &jj_m);
		t->SetBranchAddress("e_pt", &e_pt);
		t->SetBranchAddress("e_eta", &e_eta);
		t->SetBranchAddress("e_truth_type", &e_truth_type);
		t->SetBranchAddress("weight_all", &weight_all);
		t->SetBranchAddress("weight_mc", &weight_mc);
		t->SetBranchAddress("e_truth_origin", &e_truth_origin);
		TTree *t2=(TTree*)f->Get("DAOD_tree");
		t2->SetBranchAddress("dsid", &dsid);
		t2->SetBranchAddress("luminosity",&luminosity);
		t2->SetBranchAddress("xsection",&xsection);
		t2->SetBranchAddress("geneff",&geneff);
		t2->SetBranchAddress("kfactor",&kfactor);
		t2->GetEntry(0);
		bool ispj = (dsid>=364541 && dsid<=364547);
		bool ismc = (dsid > 1e5);
		bool isdata = (dsid==0);
		if(ismc && !ispj)
		{
			cout<<dsid<<endl;
			f->Close();
			continue;
		}
		int Nentry = t->GetEntries();
		for(int j=0;j<Nentry;j++)
		{
			t->GetEntry(j);
			if(abs(weight_mc*weight_all)>100)continue;
			double ew = weight_all*weight_mc*geneff*kfactor*xsection*luminosity/mapsow[int(dsid)][mappid[int(luminosity)]];
			if (isdata)ew = 1.0;
			bool pass_onegam = ((selection_bits & EventSelections::onegam )!= 0);
            bool pass_trigger = ((selection_bits & EventSelections::trigger )!= 0);
            bool pass_twojet = ((selection_bits & EventSelections::twojet )!= 0);
            bool pass_primary_vertex = ((selection_bits & EventSelections::primary_vertex )!= 0);
            bool pass_duplicate = ((selection_bits & EventSelections::duplicate )!= 0);
            bool pass_dq = ((selection_bits & EventSelections::dq )!= 0);
            bool pass_jetclean = ((selection_bits & EventSelections::jetclean )!= 0);
            bool pass_vy_OR = ((selection_bits & EventSelections::vy_OR )!= 0);
            bool is_Wenu = ((selection_bits & EventSelections::Wenu )!= 0);
            bool pass_onelep = ((selection_bits & EventSelections::onelep )!= 0);
            bool pass_second_lepton_veto = ((selection_bits & EventSelections::second_lepton_veto )!= 0);
            bool pass_ly_Z_veto = ((selection_bits & EventSelections::ly_Z_veto )!= 0);
            bool pass_dphi = ((selection_bits & EventSelections::dphi )!= 0);
            bool pass_dr = ((selection_bits & EventSelections::dr )!= 0);
            bool e_passIP = ((object_bits & ObjectSelections::bm_e_passIP )!= 0);
            bool gam_idTight = ((object_bits & ObjectSelections::bm_gam_idTight )!= 0);
            bool gam_isoTight = ((object_bits & ObjectSelections::bm_gam_isoTight )!= 0);
            bool e_isoLooseVarRad = ((object_bits & ObjectSelections::bm_e_isoLooseVarRad )!= 0);
            bool e_isoTightVarRad = ((object_bits & ObjectSelections::bm_e_isoTightVarRad )!= 0);
            bool e_idMedium = ((object_bits & ObjectSelections::bm_e_idMedium )!= 0);
            bool e_idTight = ((object_bits & ObjectSelections::bm_e_idTight )!= 0);
			bool baseline = pass_onegam && pass_trigger && pass_twojet && pass_primary_vertex && pass_second_lepton_veto && pass_dq && pass_jetclean && pass_vy_OR && is_Wenu && pass_onelep && e_pt>20.0 && abs(e_eta)<2.5 && j0_pt>30.0 && j1_pt>30.0 && met>30.0 && w_mt>30.0 && gam_pt>22.0 && gam_isoTight && gam_idTight && abs(gam_eta)<2.37 && pass_ly_Z_veto && pass_dphi && pass_dr && e_passIP;
			bool isreal = (e_truth_type >= 1 && e_truth_type <= 4);
			bool pass_signal_e = baseline && e_isoTightVarRad && e_idTight;
			if(!baseline)continue;
			if(!pass_signal_e)
			{
				pjtt->Fill(e_truth_type,ew);
				pjto->Fill(e_truth_origin,ew);
				hptb->Fill(e_pt,ew);
			}
			else
			{
				hpts->Fill(e_pt,ew);
			}
		}
		f->Close();
		//cout<<temp<<endl;
	}
	// List if looped done
	TFile *fout=new TFile("ffip_jpt30.root","RECREATE");
	TH1F *hptff=(TH1F*)hpts->Clone("hptff");
	hptff->Divide(hptb);
	pjtt->Write("pjtt");
	pjto->Write("pjto");
	hptb->Write("hptb");
	hpts->Write("hpts");
	hptff->Write("hptff");
	fout->Close();
}

void FakeManagerv08::Calfactor_Dijet(const string &list,double weight_cut)
{
	//ROOT::EnableImplicitMT();
	ROOT::DisableImplicitMT();
	auto f = [this,list,weight_cut](const double &metcut,const double &wmtcut,const double &jptcut,const double ptbin[],const int &size_ptbin)
	{
		ifstream data(list);
		unique_ptr<TH2D> h2ds ( new TH2D("h2ds", "pt_eta_signal", size_ptbin, ptbin, netabin, eta_bin));
		unique_ptr<TH2D> h2db ( new TH2D("h2db", "pt_eta_baseline", size_ptbin, ptbin, netabin, eta_bin));
		unique_ptr<TH2D> h2mcs ( new TH2D("h2mcs", "pt_eta_signal", size_ptbin, ptbin, netabin, eta_bin));
		unique_ptr<TH2D> h2mcb ( new TH2D("h2mcb", "pt_eta_baseline", size_ptbin, ptbin, netabin, eta_bin));
		unique_ptr<TH2D> h2djs ( new TH2D("h2djs", "pt_eta_signal", size_ptbin, ptbin, netabin, eta_bin));
		unique_ptr<TH2D> h2djb ( new TH2D("h2djb", "pt_eta_baseline", size_ptbin, ptbin, netabin, eta_bin));
		unique_ptr<TH1D> hdjs ( new TH1D("hdjs", "dijet signal", size_ptbin, ptbin));
		unique_ptr<TH1D> hdjb ( new TH1D("hdjb", "dijet baseline", size_ptbin, ptbin));
		unique_ptr<TH1D> hdb ( new TH1D("hdb", "dijet baseline", size_ptbin, ptbin));
		unique_ptr<TH1D> hds ( new TH1D("hds", "dijet baseline", size_ptbin, ptbin));
		unique_ptr<TH1D> hmcb ( new TH1D("hmcb", "dijet baseline", size_ptbin, ptbin));
		unique_ptr<TH1D> hmcs ( new TH1D("hmcs", "dijet baseline", size_ptbin, ptbin));
		double luminosity, xsection, geneff, kfactor, sum_of_weights;
		Double_t dsid;
		ifstream sow("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/dijet/sow.txt");
		unordered_map<int,int> mappid={{36207,0},{44307,1},{58450,2}};
		unordered_map<int,double[3]> mapsow={};
		while(!sow.eof())
		{
			int dsid1;
			double sowa,sowd,sowe;
			sow>>dsid1>>sowa>>sowd>>sowe;
			if(dsid1==0)continue;
			mapsow[dsid1][0]=sowa;
			mapsow[dsid1][1]=sowd;
			mapsow[dsid1][2]=sowe;
		}
		while(!data.eof())
		{
			string temp;
			data>>temp;
			if (temp == "")continue;
			TFile *f = TFile::Open(TString(temp),"READ");
			TTree *t = (TTree *) f->Get("dijet_tree");
			Float_t        e_pt;
			Float_t        e_eta;
			Float_t        e_phi;
			Float_t        e_e;
			Float_t        j0_pt;
			Float_t        j0_eta;
			Float_t        j0_phi;
			Float_t        j0_e;
			Int_t           j_n;
			Float_t        met;
			Float_t        w_mt;
			Int_t           pass_second_lepton_veto;
			Int_t			n_bjets;
			Int_t           e_n;
			Int_t           gam_n;
			Bool_t          e_passIP;
			Float_t        e_isoTightVarRad;
			Float_t        e_idTight;
			Double_t        weight_all;
			Double_t        weight_mc;
			Int_t           e_truth_type;
			Int_t           e_truth_origin;
			Float_t         j0lep_dr;
   Float_t         j1lep_dr;
   Float_t         j0lep_dr2;
   Float_t         j1lep_dr2;
   Float_t         j0lep_dphi;
   Float_t         j1lep_dphi;
			t->SetBranchAddress("e_truth_origin", &e_truth_origin);
			t->SetBranchAddress("e_truth_type", &e_truth_type);
			t->SetBranchAddress("weight_all", &weight_all);
			t->SetBranchAddress("weight_mc", &weight_mc);
			t->SetBranchAddress("e_idTight", &e_idTight);
			t->SetBranchAddress("e_isoTightVarRad", &e_isoTightVarRad);
			t->SetBranchAddress("e_passIP", &e_passIP);
			t->SetBranchAddress("gam_n", &gam_n);
			t->SetBranchAddress("n_bjets", &n_bjets);
			t->SetBranchAddress("e_n", &e_n);
			t->SetBranchAddress("pass_second_lepton_veto", &pass_second_lepton_veto);
			t->SetBranchAddress("met", &met);
			t->SetBranchAddress("w_mt", &w_mt);
			t->SetBranchAddress("j_n", &j_n);
			t->SetBranchAddress("j0_pt", &j0_pt);
			t->SetBranchAddress("j0_eta", &j0_eta);
			t->SetBranchAddress("j0_phi", &j0_phi);
			t->SetBranchAddress("j0_e", &j0_e);
			t->SetBranchAddress("e_eta", &e_eta);
			t->SetBranchAddress("e_phi", &e_phi);
			t->SetBranchAddress("e_e", &e_e);
			t->SetBranchAddress("e_pt", &e_pt);
			t->SetBranchAddress("j0lep_dr", &j0lep_dr);
   t->SetBranchAddress("j1lep_dr", &j1lep_dr);
   t->SetBranchAddress("j0lep_dr2", &j0lep_dr2);
   t->SetBranchAddress("j1lep_dr2", &j1lep_dr2);
   t->SetBranchAddress("j0lep_dphi", &j0lep_dphi);
   t->SetBranchAddress("j1lep_dphi", &j1lep_dphi);
			TTree *t2=(TTree*)f->Get("DAOD_tree");
			t2->SetBranchAddress("dsid", &dsid);
			t2->SetBranchAddress("luminosity",&luminosity);
			t2->SetBranchAddress("xsection",&xsection);
			t2->SetBranchAddress("geneff",&geneff);
			t2->SetBranchAddress("kfactor",&kfactor);
			t2->GetEntry(0);
			bool iswj = (dsid>=700320 && dsid<=700334);
			bool iszj = (dsid>=700335 && dsid<=700349);
			bool iswvbf = (dsid>=308096 && dsid <=308098);
			bool iszvbf = (dsid>=308092 && dsid <=308094);
			bool ismc = (dsid > 1e5);
			bool isdata = (dsid==0);
			bool isdijet = (dsid >= 364701 && dsid <= 364712);
			bool istop = (dsid == 410470 || dsid == 410471 || dsid == 500334 || dsid ==412006 || dsid == 410389) || (dsid >= 410644 && dsid <=410659) || dsid==412147 || dsid==500800 || (dsid >=502957 && dsid <=502958) || (dsid>=504553 && dsid<=504554) || dsid==504689;
			//if(!isdijet)cout<<temp<<endl;
			//f->Close();
			//continue;
			if(ismc && !isdijet && !iswj && !iszj && !istop && !iswvbf && !iszvbf)
			{
				cout<<dsid<<endl;
				f->Close();
				continue;
			}
			int Nentry = t->GetEntries();
			for(int j=0;j<Nentry;j++)
			{
				t->GetEntry(j);
				double ew = weight_all*weight_mc*geneff*kfactor*xsection*luminosity/mapsow[int(dsid)][mappid[int(luminosity)]];
				if (isdata)ew = 1.0;
				//bool pass_baseline = e_pt>20.&& j0_pt > jptcut && j0lep_dr>3.8 && j0lep_dphi>2.5 && j_n >= 1 && n_bjets==0 && met < metcut && w_mt < wmtcut && pass_second_lepton_veto && e_n == 1 && gam_n == 0 && e_passIP;
				bool pass_baseline = e_pt>20.&& j0_pt > jptcut && j0lep_dr>3.8 && j0lep_dphi>2.5 && j_n >= 1 && n_bjets==0 && met < metcut && w_mt < wmtcut && pass_second_lepton_veto && e_n == 1 && gam_n == 0 && e_passIP;
				bool pass_signal = pass_baseline && e_isoTightVarRad && e_idTight;
				if(!pass_baseline||int(dsid)==364701||ew>weight_cut)continue;
				if(!pass_signal) {
					if (ismc) {
						if (isdijet) {
							h2djb->Fill(e_pt,abs(e_eta),ew);
							hdjb->Fill(e_pt,ew);
						}
						else {
							h2mcb->Fill(e_pt,abs(e_eta),ew);
							hmcb->Fill(e_pt,ew);
						}
					} else {
						h2db->Fill(e_pt, abs(e_eta), 1);
						hdb->Fill(e_pt,ew);
					}
				}
				else{
					if (ismc) {
						if (isdijet) {
							h2djs->Fill(e_pt,abs(e_eta),ew);
							hdjs->Fill(e_pt,ew);
						}
						else{
							h2mcs->Fill(e_pt,abs(e_eta),ew);
							hmcs->Fill(e_pt,ew);
						}
					} else {
						h2ds->Fill(e_pt,abs(e_eta),1);
						hds->Fill(e_pt,ew);
					}
				}
			}
			f->Close();
			//cout<<temp<<endl;
		}
		// List if looped done
		TString outname = TString("fakefactor_met") + TString(to_string((int)metcut)) + TString("_wmt") + TString(to_string((int)wmtcut)) + TString("_jpt") + TString(to_string((int)jptcut)) + TString(".root");
		if(size_ptbin!=3)outname=TString("fakefactor_npt") + TString(to_string(size_ptbin)) + TString(".root");
		unique_ptr<TH2D> hnum=make_unique<TH2D>(*(TH2D*)h2ds.get()->Clone()); 
		unique_ptr<TH2D> hden=make_unique<TH2D>(*(TH2D*)h2db.get()->Clone());
		hnum->Add(h2mcs.get(),-1);
		hden->Add(h2mcb.get(),-1);
		unique_ptr<TH1D> hfptdj=make_unique<TH1D> (*(TH1D*)hdjs.get()->Clone());
		hfptdj->Divide(hdjb.get());
		unique_ptr<TH2D> h2eff =make_unique<TH2D> (*(TH2D*)hnum.get()->Clone());
		h2eff->Divide(hden.get());
		unique_ptr<TH1D>h1num=make_unique<TH1D>(*(TH1D*)hds.get()->Clone());
		unique_ptr<TH1D>h1den=make_unique<TH1D>(*(TH1D*)hdb.get()->Clone());
		h1num->Add(hmcs.get(),-1);
		h1den->Add(hmcb.get(),-1);
		unique_ptr<TH1D>hfpt = make_unique<TH1D>(*(TH1D*)h1num.get()->Clone());
		hfpt->Divide(h1den.get());
		TFile *fout=new TFile(outname,"RECREATE");
		fout->cd();
		h2eff->Write("h2eff");
		h2ds->Write("h2ds");
		h2db->Write("h2db");
		h2djs->Write("h2djs");
		h2djb->Write("h2djb");
		h2mcs->Write("h2mcs");
		h2mcb->Write("h2mcb");
		hfptdj->Write("hfptdj");
		hdjs->Write("hdjs");
		hdjb->Write("hdjb");
		hmcs->Write("hmcs");
		hmcb->Write("hmcb");
		hds->Write("hds");
		hdb->Write("hdb");
		hfpt->Write("hfpt");
		fout->Close();
		hdjs.reset();
		cout<<outname<<" produced !"<<endl;
	};
	//ROOT::EnableThreadSafety();
	//for(int ith=0;ith<8;ith++)
    //{
    //    async(launch::async,f,20.,20.,30.,pt_bin,nptbin);
    //}
    //async(launch::async,f,20.,20.,30.,pt_bin,nptbin);
    //async(launch::async,f,15.,20.,30.,pt_bin,nptbin);
    //async(launch::async,f,25.,20.,30.,pt_bin,nptbin);
    //async(launch::async,f,20.,15.,30.,pt_bin,nptbin);
    //async(launch::async,f,20.,25.,30.,pt_bin,nptbin);
    //async(launch::async,f,20.,20.,25.,pt_bin,nptbin);
    //async(launch::async,f,20.,20.,35.,pt_bin,nptbin);
    //async(launch::async,f,20.,20.,30.,pt_bin9,9);
	f(20.,20.,30.,pt_bin,nptbin);
	f(15.,20.,30.,pt_bin,nptbin);
	f(25.,20.,30.,pt_bin,nptbin);
	f(20.,15.,30.,pt_bin,nptbin);
	f(20.,25.,30.,pt_bin,nptbin);
	f(20.,20.,25.,pt_bin,nptbin);
	f(20.,20.,35.,pt_bin,nptbin);
	f(20.,20.,30.,pt_bin9,9);
}

void FakeManagerv08::Syst()
{
	//7 values need to be stored
	//ROOT::EnableImplicitMT();
	//ROOT::DisableImplicitMT();
	cout<<"Syst root file section starting"<<endl;
	vector<TVectorD> plotsr,plotcra,plotcrb,plotcrc;
	auto f_gethist = [](TString fname,TString region)->TH1D*
	{
		TFile *f=TFile::Open(fname,"READ");
		TH1D *h=(TH1D*)f->Get("hfake"+region);
		h->SetDirectory(0);
		f->Close();
		delete f;
		return h;
	};
	auto f_gethist_from_list = [](const vector<TString>& flist,const TString& region,TH1D* hnominal)->void
	//auto f_gethist_from_list = [](const string &flist,const TString& region,TH1D* hnominal)->void
	{
		double *vec_uncer = new double[hnominal->GetNbinsX()];
		for(int i=0;i<hnominal->GetNbinsX();i++)
		{
			vec_uncer[i]=0.;
		}
		//ifstream data;
		//data.open(flist,ios::in);
		for(int i=0;i<flist.size();i++)
		//while(!data.eof())
		{
			//string temp;
			//data>>temp;
			//if(temp=="")continue;
			//cout<<temp<<endl;
			TFile *f=TFile::Open(flist.at(i),"READ");
			//TFile *f=TFile::Open(TString(temp),"READ");
			if(!f->IsOpen())
			{
				cout<<"Cannot open file "<<flist.at(i)<<endl;
				return;
			}
			TH1D *h=(TH1D*)f->Get("hfake"+region)->Clone();
			h->SetDirectory(0);
			for(int ibin=0;ibin<hnominal->GetNbinsX();ibin++)
			{
				double diff_bin = h->GetBinContent(ibin+1)-hnominal->GetBinContent(ibin+1);
				if(abs(diff_bin)>abs(vec_uncer[ibin]))vec_uncer[ibin]=diff_bin;
			}
			f->Close();
			delete f;
		}
		for(int ibin=0;ibin<hnominal->GetNbinsX();ibin++)
		{
			hnominal->SetBinContent(ibin+1,hnominal->GetBinContent(ibin+1)+vec_uncer[ibin]);
		}
	};
	vector<TString> Region={"sr","cra","crb","crc"};
	vector<TString> scale_list,pdf_list;
	scale_list.clear();
	pdf_list.clear();
	for(int i=5;i<=17;i+=2)scale_list.push_back("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/fake/scale_pdf/"+TString(_varname)+"/jobs/"+TString(to_string(i))+"/fake_scale_"+TString(to_string(i))+".root");
	for(int i=19;i<=317;i+=2)pdf_list.push_back("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/fake/scale_pdf/"+TString(_varname)+"/jobs/"+TString(to_string(i))+"/fake_pdf_"+TString(to_string(i))+".root");
	cout<<scale_list.size()<<" "<<pdf_list.size()<<endl;
	TFile *fout=new TFile(TString(_varname)+"_fake_syst.root","RECREATE");
	fout->cd();
	for(auto reg:Region)
	{
		TH1D* h_nominal=f_gethist("fake_nominal.root",reg);
		TH1D* h_gamjetup=f_gethist("fake_gamjetup.root",reg);
		TH1D* h_gamjetdown=f_gethist("fake_gamjetdown.root",reg);
		TH1D* h_varup=f_gethist("fake_varup.root",reg);
		TH1D* h_vardown=f_gethist("fake_vardown.root",reg);
		TH1D* h_npt9=f_gethist("fake_npt9.root",reg);
		TH1D* h_qcd=f_gethist("fake_qcd.root",reg);
		TH1D* h_scale=f_gethist("fake_nominal.root",reg);
		f_gethist_from_list(scale_list,reg,h_scale);
		TH1D* h_pdf=f_gethist("fake_nominal.root",reg);
		f_gethist_from_list(pdf_list,reg,h_pdf);
		fout->cd();
		h_nominal->Write("jfakee_nominal_"+reg);
		h_gamjetup->Write("jfakee_gamjetup_"+reg);
		h_gamjetdown->Write("jfakee_gamjetdown_"+reg);
		h_varup->Write("jfakee_varup_"+reg);
		h_vardown->Write("jfakee_vardown_"+reg);
		h_npt9->Write("jfakee_bin_"+reg);
		h_qcd->Write("jfakee_qcd_"+reg);
		h_scale->Write("jfakee_scale_"+reg);
		h_pdf->Write("jfakee_pdf_"+reg);
	}
	fout->Close();
}

void FakeManagerv08::Plot()
{
	TFile *f=TFile::Open(TString(_varname)+"_fake_syst.root","READ");
	vector<TString> Region={"sr","cra","crb","crc"};
	vector<TVectorD> _value; // systematic uncertainty for each bin
	//Function to get the envelope of bin i
	auto f_envelope = [](TH1D *h1,TH1D *hup,TH1D *hdown,int ibin)->double
	{
		double dif1 = abs(hup->GetBinContent(ibin+1)-h1->GetBinContent(ibin+1));
		double dif2 = abs(hdown->GetBinContent(ibin+1)-h1->GetBinContent(ibin+1));
		return dif1 > dif2 ? dif1 : dif2;
	};
	for(auto reg:Region)
	{
		_value.clear();
		TH1D* h_nominal=(TH1D*)f->Get("jfakee_nominal_"+reg);
		TH1D* h_gamjetup=(TH1D*)f->Get("jfakee_gamjetup_"+reg);
		TH1D* h_gamjetdown=(TH1D*)f->Get("jfakee_gamjetdown_"+reg);
		TH1D* h_varup=(TH1D*)f->Get("jfakee_varup_"+reg);
		TH1D* h_vardown=(TH1D*)f->Get("jfakee_vardown_"+reg);
		TH1D* h_npt9=(TH1D*)f->Get("jfakee_bin_"+reg);
		TH1D* h_QCD=(TH1D*)f->Get("jfakee_qcd_"+reg);
		TH1D* h_scale_alone=(TH1D*)f->Get("jfakee_scale_"+reg);
		TH1D* h_pdf_alone=(TH1D*)f->Get("jfakee_pdf_"+reg);
		if(_value.size()==0)
		{
			for(int i=0;i<h_nominal->GetNbinsX();i++)
			{
				_value.push_back(TVectorD(9));//9 is the number of systematics including nominal
			}
		}
		for(int ibin=0;ibin<h_nominal->GetNbinsX();ibin++)//_v is TVectorD 
		{
			_value.at(ibin)[0] = h_nominal->GetBinContent(ibin+1);
			_value.at(ibin)[1] = h_nominal->GetBinError(ibin+1);
			_value.at(ibin)[2] = f_envelope(h_nominal,h_gamjetup,h_gamjetdown,ibin);
			_value.at(ibin)[3] = f_envelope(h_nominal,h_QCD,h_nominal,ibin);
			_value.at(ibin)[4] = f_envelope(h_nominal,h_varup,h_vardown,ibin);
			_value.at(ibin)[5] = f_envelope(h_nominal,h_npt9,h_nominal,ibin);
			_value.at(ibin)[6] = f_envelope(h_nominal,h_scale_alone,h_nominal,ibin);
			_value.at(ibin)[7] = f_envelope(h_nominal,h_pdf_alone,h_nominal,ibin);
			for(int k=1;k<8;k++)
			{
				_value.at(ibin)[8] += pow(_value.at(ibin)[k],2);
			}
			_value.at(ibin)[8] = sqrt(_value.at(ibin)[8]);
		}
		cout<<reg<<endl;
		int width = 12;
		for(int ibin=0;ibin<_value.size();ibin++)
		{
			cout<<left<<"Bin"<<ibin+1<<": ";
			for(int k=0;k<9;k++)
			{
				cout<<left<<setw(width)<<_value.at(ibin)[k];
			}
			cout<<endl;
		}
	// Now start plotting
		TCanvas *c=new TCanvas("c","test",1024,768);
		TLegend *leg=new TLegend(0.6,0.7,0.9,0.9);
		//Prepare canvas
		c->SetBorderMode(0);
		c->SetBorderSize(2);
		c->SetTickx(1);
		c->SetTicky(1);
		gStyle->SetOptTitle(0);
		c->SetLeftMargin(0.16);
		c->SetRightMargin(0.05);
		c->SetTopMargin(0.05);
		c->SetBottomMargin(0.16);
		c->SetFrameBorderMode(0);
		c->SetFrameBorderMode(0);
		leg->SetBorderSize(0);
		leg->SetTextSize(0.04);
		leg->SetLineColor(0);
		leg->SetLineStyle(1);
		leg->SetLineWidth(3);
		leg->SetFillColor(0);
		leg->SetFillStyle(0);
		c->cd();
		gStyle->SetOptStat("");
		TH1D *h_bin = (TH1D*)h_nominal->Clone("h_bin");
		TH1D *h_sel = (TH1D*)h_nominal->Clone("h_sel");
		TH1D *h_qcd = (TH1D*)h_nominal->Clone("h_qcd");
		TH1D *h_gamjet = (TH1D*)h_nominal->Clone("h_gamjet");
		TH1D *h_scale = (TH1D*)h_nominal->Clone("h_scale");
		TH1D *h_pdf = (TH1D*)h_nominal->Clone("h_pdf");
		for(int ibin=0;ibin<h_nominal->GetNbinsX();ibin++)
		{
			h_pdf->SetBinError(ibin+1,_value.at(ibin)[8]);
			h_scale->SetBinError(ibin+1,sqrt(pow(_value.at(ibin)[8],2)-pow(_value.at(ibin)[7],2)));
			h_bin->SetBinError(ibin+1,sqrt(pow(h_scale->GetBinError(ibin+1),2)-pow(_value.at(ibin)[6],2)));
			h_sel->SetBinError(ibin+1,sqrt(pow(h_bin->GetBinError(ibin+1),2)-pow(_value.at(ibin)[5],2)));
			h_qcd->SetBinError(ibin+1,sqrt(pow(h_sel->GetBinError(ibin+1),2)-pow(_value.at(ibin)[4],2)));
			h_gamjet->SetBinError(ibin+1,sqrt(pow(h_qcd->GetBinError(ibin+1),2)-pow(_value.at(ibin)[3],2)));
			if((sqrt(pow(h_gamjet->GetBinError(ibin+1),2)-pow(_value.at(ibin)[2],2))-h_nominal->GetBinError(ibin+1))>0.01)
			{
				cout<<"Error: "<<sqrt(pow(h_gamjet->GetBinError(ibin+1),2)-pow(_value.at(ibin)[2],2))<<" != "<<h_nominal->GetBinError(ibin+1)<<endl;
			}
		}
		h_bin->SetFillColor(kAzure);
		h_sel->SetFillColor(kAzure+4);
		h_qcd->SetFillColor(kPink+9);
		h_gamjet->SetFillColor(kCyan+1);
		h_bin->SetFillStyle(3335);
		h_sel->SetFillStyle(3353);
		h_qcd->SetFillStyle(3305);
		h_gamjet->SetFillStyle(3395);
		h_nominal->SetFillColor(kGray+1);
		leg->AddEntry(h_nominal,"Statistic");
		leg->AddEntry(h_gamjet,"Syst. Gamjet Closure");
		leg->AddEntry(h_qcd,"Syst. QCD Variation");
		leg->AddEntry(h_sel,"Syst. Selection");
		leg->AddEntry(h_bin,"Syst. Binning");
		leg->AddEntry(h_scale,"Syst. Scale");
		leg->AddEntry(h_pdf,"Syst. PDF");
		h_pdf->Draw("e2");
		h_scale->Draw("e2same");
		h_bin->Draw("e2same");
		h_sel->Draw("e2same");
		h_qcd->Draw("e2same");
		h_gamjet->Draw("e2same");
		h_nominal->Draw("e2same");
		h_nominal->SetLineColor(1);
		h_nominal->SetLineWidth(2);
		h_nominal->Draw("e1same");
		leg->Draw();
		int tex_x = h_nominal->GetXaxis()->GetXmin()+(h_nominal->GetXaxis()->GetXmax()-h_nominal->GetXaxis()->GetXmin())*0.58;
		int tex_x2 = h_nominal->GetXaxis()->GetXmin()+(h_nominal->GetXaxis()->GetXmax()-h_nominal->GetXaxis()->GetXmin())*0.74;
		int tex_y = h_nominal->GetMinimum()+(h_nominal->GetMaximum()-h_nominal->GetMinimum())*0.9;
		TLatex *   tex = new TLatex(tex_x,tex_y,"ATLAS");
        tex->SetTextFont(72);
        tex->SetLineWidth(2);
        tex->SetTextSize(0.045);
        tex->Draw();
        tex = new TLatex(tex_x2,tex_y,"Internal");
        tex->SetTextFont(42);
        tex->SetLineWidth(2);
        tex->SetTextSize(0.045);
        tex->Draw();
		c->SaveAs(TString(_varname)+"_"+reg+".png");
		
	}
}

FakeManagerv08::FakeManagerv08()
{}

FakeManagerv08::~FakeManagerv08()
{}
