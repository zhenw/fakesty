#include <fstream>
#include <iostream>
#include <string>
#include "FakeManager.h"
#include "FakeManagerv08.h"

using namespace std;

int main(int argc,char* argv[])
{
    time_t time1,time2;
    clock_t startTime,endTime;
    float diff_time;
    time(&time1);
    startTime = clock();
// main body
	string var;
	int ifplot=0,iffake=0,version=0,sp_index=0;
	for(int i=1;i<argc;i++)
	{
		if(string(argv[i])=="-v")
		{
			var = string(argv[i+1]);
		}
		else if(string(argv[i])=="-p")
		{
			ifplot=1;
		}
		else if(string(argv[i])=="-f")
		{
			iffake=1;
		}
		else if(string(argv[i])=="-n")
		{
			version= stoi(argv[i+1]);
		}
		else if(string(argv[i])=="-sp")
		{
			sp_index= stoi(argv[i+1]);
		}
	}
	double bin_jj_m[5]={1000., 1450., 1800., 2300., 5300.};
	double bin_ngapjets[9]={0., 1., 2., 3.,4., 5.,6.,7.,8.};
	double bin_nn[6]={0.0,0.2,0.4,0.6,0.8,1.0};
	double bin_jj_pt[6]={0.,80.,125.,180.,270.,840.};
	double bin_jj_dphi[9] = {-3.1416, -2.9452, -2.7489, -2.4435, 0.0, 2.4435, 2.7489, 2.9452, 3.1416};
	string listv08="/lustre/collider/wangzhen/atlas/vbs/fake_v08/minitree_v08/vbswy/listv08.txt";
	string listv08_mjj1k="/lustre/collider/wangzhen/atlas/vbs/fake_v08/minitree_v08/vbswy_mjj1k/listv08_mjj1k.txt";
	string listv07="listmini.txt";
	unordered_map<string,double*> map_bin=
	{
		{"jj_m",bin_jj_m},
		{"ngapjets",bin_ngapjets},
		{"nn",bin_nn},
		{"jj_pt",bin_jj_pt},
		{"jj_dphi_signed",bin_jj_dphi},
	};
	unordered_map<string,int> map_nbin=
	{
		{"jj_m",4},
		{"ngapjets",8},
		{"nn",5},
		{"jj_pt",5},
		{"jj_dphi_signed",8},
	};
	if(version==7)
	{
		FakeManager fakeManager;
		//fakeManager.Calfactor_Dijet("listdj.txt",15000);
		//TFile *fgam=TFile::Open("ffip_jpt30.root","READ");
		//TH1F *hgam=(TH1F*)fgam->Get("hptff");
		//double diff=10000.;
		//for(double cut=50000;cut>1000;cut-=1000)
		//{
		//	fakeManager.Calfactor_Dijet("listmcdj.txt",cut);
		//	TFile *fdj=TFile::Open("fakefactor_met20_wmt20_jpt30.root","READ");
		//	TH1D *hdj=(TH1D*)fdj->Get("hfptdj");
		//	double diff_tmp=0;
		//	for(int i=1;i<hdj->GetNbinsX();i++)
		//	{
		//		diff_tmp+=abs(hdj->GetBinContent(i)-hgam->GetBinContent(i));
		//	}
		//	if(diff_tmp<diff)
		//	{
		//		diff=diff_tmp;
		//		cout<<cut<<" "<<diff<<endl;
		//	}
		//}
		//fakeManager.Calfactor_Gamjet("listmcyj.txt");
		cout<<var<<endl;
		fakeManager.SetVar(var);
		if(iffake)
		{
			fakeManager.Fake(listv07,var,map_nbin[var],map_bin[var]);
			fakeManager.Syst();
		}
		if(ifplot)
		{
			fakeManager.Plot();
		}
	}
	else if(version==8)
	{
		FakeManagerv08 fakeManagerv08;
		//fakeManagerv08.Calfactor_Dijet("/lustre/collider/wangzhen/atlas/vbs/fake_v08/minitree_v08/dijetbkg/listdjv08.txt",2000);
		//TFile *fgam=TFile::Open("/lustre/collider/wangzhen/atlas/vbs/fake_v08/Fakesty/run_v08/gamjet/v8/ffip_jpt30.root","READ");
		//TH1F *hgam=(TH1F*)fgam->Get("hptff");
		//double diff=10000.;
		//for(double cut=50000;cut>1000;cut-=1000)
		//{
		//	fakeManagerv08.Calfactor_Dijet("/lustre/collider/wangzhen/atlas/vbs/fake_v08/minitree_v08/dijetmc2/listdjv082.txt",cut);
		//	TFile *fdj=TFile::Open("fakefactor_met20_wmt20_jpt30.root","READ");
		//	TH1D *hdj=(TH1D*)fdj->Get("hfptdj");
		//	double diff_tmp=0;
		//	for(int i=1;i<hdj->GetNbinsX();i++)
		//	{
		//		diff_tmp+=abs(hdj->GetBinContent(i)-hgam->GetBinContent(i));
		//	}
		//	if(diff_tmp<diff)
		//	{
		//		diff=diff_tmp;
		//		cout<<cut<<" "<<diff<<endl;
		//	}
		//	else
		//	{
		//		cout<<"current cut: "<<cut<<" diff: "<<diff_tmp<<"max: "<<cut<<" diff: "<<diff<<endl;
		//	}
		//}
		//fakeManagerv08.Calfactor_Gamjet("/lustre/collider/wangzhen/atlas/vbs/fake_v08/minitree_v08/gamjet/listyjv08.txt");
		cout<<var<<endl;
		fakeManagerv08.SetVar(var);
		if(iffake)
		{
			if(sp_index==0)fakeManagerv08.Fake(listv08_mjj1k,var,map_nbin[var],map_bin[var]);
			else
			{
				fakeManagerv08.Fake(listv08_mjj1k,var,map_nbin[var],map_bin[var],1,sp_index);
			}
		}
		fakeManagerv08.Syst();
		if(ifplot)
		{
			fakeManagerv08.Plot();
		}
	}

    endTime = clock();
    time(&time2);
    diff_time = difftime(time2,time1);
    cout<<"Running(CPU) time: "<<(double)(endTime - startTime) / CLOCKS_PER_SEC<<" s."<<endl;
    cout<<"Actual time: "<<diff_time<<" s."<<endl;
	return 1;
}
